﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EVE.ApiIntegration;

namespace Eve.ApiIntegration.Tests
{
    [TestClass]
    public class Authentication
    {

        [TestMethod]
        public void KeyInformationInvalidKey()
        {
            ApiClient client = new ApiClient("test@mail.com", "https://api.testeveonline.com/");

            ApiBase<ApiKeyInfo> info = client.Execute<ApiKeyInfo>(EndpointType.Account, Endpoint.ApiKeyInfo, TestInfo.TestApiKey);

            Assert.IsNotNull(info);
            Assert.IsNull(info.Result, "Result is null");
        }
    }
}
