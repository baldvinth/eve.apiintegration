﻿using EVE.ApiIntegration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.ApiIntegration.Tests {
    public static class TestInfo {
        public static ApiKey TestApiKey{ get; set; }
        public static long TestCharacterId { get; set; }
        public static List<long> TestCharacterIdList { get; set; }
        public static string TestEmail { get; set; }
        public static string TestBaseUrl { get; set; }
    }
}
