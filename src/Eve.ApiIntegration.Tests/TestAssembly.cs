﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace Eve.ApiIntegration.Tests {
    [TestClass]
    public class TestAssembly {

        public const string TESTINFO_TESTAPIKEY_KEYID_KEY = "TestInfo.TestApiKey.KeyId";
        public const string TESTINFO_TESTAPIKEY_VERIFICATION_KEY = "TestInfo.TestApiKey.VerificationCode";
        public const string TESTINFO_CHARACTERID_KEY = "TestInfo.CharacterId";
        public const string TESTINFO_CHARACTERIDLIST_KEY = "TestInfo.CharacterIdList";
        public const string TESTINFO_EMAIL_KEY = "TestInfo.Email";
        public const string TESTINFO_BASEURL_KEY = "TestInfo.BaseUrl";

        [AssemblyInitialize]
        public static void Initialize(TestContext ctx) {
            //TODO: Move this initialization to either IOC container and/or implement Mock framework
            TestInfo.TestApiKey = new EVE.ApiIntegration.ApiKey();
            string _keyIdStr = null;
            long _keyIdValue = -1;
            if (!((_keyIdStr = ConfigurationManager.AppSettings[TESTINFO_TESTAPIKEY_KEYID_KEY]) != null
                && long.TryParse(_keyIdStr, out _keyIdValue)
                && (TestInfo.TestApiKey.KeyId = _keyIdValue) > 0)) {
                //TODO: Add error logging
            }
            string _verificationCodeValue = null;
            if (!((_verificationCodeValue = ConfigurationManager.AppSettings[TESTINFO_TESTAPIKEY_VERIFICATION_KEY]) != null
                && (!String.IsNullOrEmpty(TestInfo.TestApiKey.VerificationCode= _verificationCodeValue)))) {
                //TODO: Add error logging
            }
            string _testCharIdStr = null;
            long _testCharIdValue = -1;
            if (!((_testCharIdStr = ConfigurationManager.AppSettings[TESTINFO_CHARACTERID_KEY]) != null
                && long.TryParse(_testCharIdStr, out _testCharIdValue)
                && (TestInfo.TestCharacterId = _testCharIdValue) > 0)) {
                //TODO: Add error logging
            }
            string _testCharIdListStr = null;
            string[] testCharIdList = null;
            if (((_testCharIdListStr = ConfigurationManager.AppSettings[TESTINFO_CHARACTERIDLIST_KEY]) != null)) {
                testCharIdList = _testCharIdListStr.Split(',');
                if (testCharIdList.Length > 0) {
                    TestInfo.TestCharacterIdList = new List<long>();
                    long charId = -1;
                    foreach (var charIdStr in testCharIdList) {
                        if (long.TryParse(charIdStr, out charId))
                            TestInfo.TestCharacterIdList.Add(charId);
                    }
                }
                //TODO: Add error logging
            }
            string _emailValue = null;
            if (!((_emailValue = ConfigurationManager.AppSettings[TESTINFO_EMAIL_KEY]) != null
                && (!String.IsNullOrEmpty(TestInfo.TestEmail = _emailValue)))) {
                //TODO: Add error logging
            }
            string _baseUrlValue = null;
            if (!((_baseUrlValue = ConfigurationManager.AppSettings[TESTINFO_BASEURL_KEY]) != null
                && (!String.IsNullOrEmpty(TestInfo.TestBaseUrl = _baseUrlValue)))) {
                //TODO: Add error logging
            }
        }

    }
}
