﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EVE.ApiIntegration;
using System.Threading.Tasks;

namespace Eve.ApiIntegration.Tests
{
    [TestClass]
    public class ServerTests
    {
        [TestMethod]
        public void GetServerStatus()
        {
            ApiClient client = new ApiClient("test@mail.com", "https://api.testeveonline.com/");

            ApiBase<ServerStatus> status = client.Execute<ServerStatus>(EndpointType.Server, Endpoint.ServerStatus);

            Assert.IsNotNull(status);
            Assert.IsNotNull(status.Result);
            Assert.IsTrue(status.Result.OnlinePlayers >= 0, string.Format("There are {0} users on the server", status.Result.OnlinePlayers));
        }

        [TestMethod]
        public async Task GetServerStatusAsync()
        {
            ApiClient client = new ApiClient("test@mail.com", "https://api.testeveonline.com/");

            ApiBase<ServerStatus> status = await client.ExecuteAsync<ServerStatus>(EndpointType.Server, Endpoint.ServerStatus);

            Assert.IsNotNull(status);
            Assert.IsNotNull(status.Result);
            Assert.IsTrue(status.Result.OnlinePlayers >= 0, string.Format("There are {0} users on the server", status.Result.OnlinePlayers));
        }
    }
}
