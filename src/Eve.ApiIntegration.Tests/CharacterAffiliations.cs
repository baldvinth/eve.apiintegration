﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EVE.ApiIntegration;
using EVE.ApiIntegration.Models.Eve;
using System.Collections.Generic;

namespace Eve.ApiIntegration.Tests {

    [TestClass]
    public class CharacterAffiliations {

        [TestMethod]
        public void GetCharacterAffiliationInformation() {
            ApiClient client = new ApiClient(TestInfo.TestEmail, TestInfo.TestBaseUrl);

            ApiBase<CharacterAffiliation> info = client.Execute<CharacterAffiliation>(new List<long>(new long[] { TestInfo.TestCharacterId }),
                EndpointType.Eve,
                Endpoint.CharacterAffiliation);

            Assert.IsNotNull(info);
            Assert.IsNotNull(info.Result, "Result is null");
            Assert.AreNotEqual(0, info.Result.CharacterID, "CharacterId should be greater than 0");
        }

        //[TestMethod]
        public void GetCharacterAffiliationList() {
            ApiClient client = new ApiClient(TestInfo.TestEmail, TestInfo.TestBaseUrl);

            ApiBase<List<CharacterAffiliation>> info = client.Execute<List<CharacterAffiliation>>(TestInfo.TestCharacterIdList,
                EndpointType.Eve,
                Endpoint.CharacterAffiliation);

            Assert.IsNotNull(info);
            Assert.IsNotNull(info.Result, "List<T> Result is null");
            Assert.AreNotEqual(0, info.Result.Count, "Not found!");
            Assert.IsTrue(info.Result.Count > 1, "List<T> contains too few items!");
        }

        [TestMethod]
        public void GetCustomCharacterAffiliation() {
            ApiClient client = new ApiClient(TestInfo.TestEmail, TestInfo.TestBaseUrl);

            CharacterAffiliation info = CharacterAffiliation.Get(TestInfo.TestCharacterId);

            Assert.IsNotNull(info);
            Assert.AreNotEqual(0, info.CharacterID, "CharacterId should be greater than 0");
        }

        [TestMethod]
        public void GetCustomCharacterAffiliationList() {
            ApiClient client = new ApiClient(TestInfo.TestEmail, TestInfo.TestBaseUrl);

            List<CharacterAffiliation> info = CharacterAffiliation.Get(TestInfo.TestCharacterIdList);

            Assert.IsNotNull(info);
            Assert.IsTrue(info.Count > 1, "List<T> contains too few items!");
        }
    }
}
