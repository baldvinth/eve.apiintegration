﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EVE.ApiIntegration;

namespace Eve.ApiIntegration.Tests {

    [TestClass]
    public class Characters {

        [TestMethod]
        public void GetCharacterInformation() {
            ApiClient client = new ApiClient(TestInfo.TestEmail, TestInfo.TestBaseUrl);

            ApiBase<Character> info = client.Execute<Character>(EndpointType.Char, 
                Endpoint.CharacterSheet, 
                TestInfo.TestApiKey, 
                TestInfo.TestCharacterId);

            Assert.IsNotNull(info);
            Assert.IsNotNull(info.Result, "Result is null");
            Assert.AreNotEqual(0, info.Result.CharacterId, "CharacterId should be greater than 0");
        }
    }
}
