﻿using EVE.ApiIntegration.Utils;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EVE.ApiIntegration
{
    public class ApiClient
    {
        private string _userAgent = "EVE.ApiIntegration/0.1.0 Contact/{0}";
        private string _baseUrl = string.Empty;

        /// <summary>
        /// Initialize the API client using an e-mail stored in web.config
        /// </summary>
        public ApiClient()
        {
            _userAgent = string.Format(_userAgent, ConfigurationHelper.GetAppSettingEmail("EVE.ApiIntegration.Email"));
            _baseUrl = ConfigurationHelper.GetAppSetting("EVE.ApiIntegration.Url");
        }

        /// <summary>
        /// Initialize the API client using an e-mail address supplied via string.
        /// </summary>
        /// <param name="email"></param>
        public ApiClient(string email, string baseUrl = "")
        {
            _userAgent = string.Format(_userAgent, email);
            _baseUrl = string.IsNullOrEmpty(baseUrl) ? ConfigurationHelper.GetAppSetting("EVE.ApiIntegration.Url") : baseUrl;
        }

        /// <summary>
        /// Call to the API endpoint of your choice
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="endpoint"></param>
        /// <param name="key"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public ApiBase<T> Execute<T>(EndpointType type, Endpoint endpoint, ApiKey key, long? characterId) {
            RestRequest request = new RestRequest();

            // Request information
            request.Method = Method.GET;
            request.RequestFormat = DataFormat.Xml;
            request.AddHeader("Content-Type", "application/xml");
            request.AddHeader("User-Agent", _userAgent);

            // Build the request URL
            string requestUrl = string.Format("{0}/{1}/{2}.xml.aspx?", _baseUrl.TrimEnd('/'), type, endpoint);

            // Make sure we have a valid API key
            if (key != default(ApiKey)) {
                request.AddParameter("keyId", key.KeyId);
                request.AddParameter("vCode", key.VerificationCode);
            }

            // Was a character ID supplied?
            if (characterId.HasValue) {
                request.AddParameter("characterId", characterId.Value);
            }

            RestClient client = new RestClient(requestUrl);

            client.AddHandler("text/xml", new Utils.RestSharp.XmlAttributeDeserializer());
            client.AddHandler("application/xml", new Utils.RestSharp.XmlAttributeDeserializer());

            IRestResponse<ApiBase<T>> response = client.Execute<ApiBase<T>>(request);

            if (response.ErrorException != null || !string.IsNullOrEmpty(response.ErrorMessage)) {
                throw new Exception(string.Format("Exception occurred while calling the EVE API. Endpoint: {0} || Error message: {1}", requestUrl, response.ErrorMessage), response.ErrorException);
            }

            if (response != null && response.Data != null) {
                return response.Data;
            }

            return default(ApiBase<T>);
        }

        /// <summary>
        /// Call to the API endpoint of your choice with list of character ids
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="endpoint"></param>
        /// <param name="key"></param>
        /// <param name="characterIds"></param>
        /// <returns></returns>
        public ApiBase<T> Execute<T>(List<long> characterIds, EndpointType type, Endpoint endpoint, ApiKey key) {
            RestRequest request = new RestRequest();

            // Request information
            request.Method = Method.GET;
            request.RequestFormat = DataFormat.Xml;
            request.AddHeader("Content-Type", "application/xml");
            request.AddHeader("User-Agent", _userAgent);

            // Build the request URL
            string requestUrl = string.Format("{0}/{1}/{2}.xml.aspx?", _baseUrl.TrimEnd('/'), type, endpoint);

            // Make sure we have a valid API key
            if (key != default(ApiKey)) {
                request.AddParameter("keyId", key.KeyId);
                request.AddParameter("vCode", key.VerificationCode);
            }

            // Was a list of character IDs supplied?
            if (characterIds != null && characterIds.Count > 0) {
                request.AddParameter("ids", String.Join(",",characterIds));
            }

            RestClient client = new RestClient(requestUrl);

            client.AddHandler("text/xml", new Utils.RestSharp.XmlAttributeDeserializer());
            client.AddHandler("application/xml", new Utils.RestSharp.XmlAttributeDeserializer());

            IRestResponse<ApiBase<T>> response = client.Execute<ApiBase<T>>(request);

            if (response.ErrorException != null || !string.IsNullOrEmpty(response.ErrorMessage)) {
                throw new Exception(string.Format("Exception occurred while calling the EVE API. Endpoint: {0} || Error message: {1}", requestUrl, response.ErrorMessage), response.ErrorException);
            }

            if (response != null && response.Data != null) {
                return response.Data;
            }

            return default(ApiBase<T>);
        }

        public ApiBase<T> Execute<T>(EndpointType type, Endpoint endpoint, ApiKey key)
        {
            return Execute<T>(type, endpoint, key, null);
        }

        public ApiBase<T> Execute<T>(EndpointType type, Endpoint endpoint, long characterId)
        {
            return Execute<T>(type, endpoint, null, characterId);
        }

        public ApiBase<T> Execute<T>(List<long> ids, EndpointType type, Endpoint endpoint) {
            return Execute<T>(ids, type, endpoint, null);
        }
        public ApiBase<T> Execute<T>(EndpointType type, Endpoint endpoint)
        {
            return Execute<T>(type, endpoint, null, null);
        }

        /// <summary>
        /// Asynchronous call to the API endpoint of your choice
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="endpoint"></param>
        /// <param name="key"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public async Task<ApiBase<T>> ExecuteAsync<T>(EndpointType type, Endpoint endpoint, ApiKey key, long? characterId)
        {
            RestRequest request = new RestRequest();

            // Request information
            request.Method = Method.GET;
            request.RequestFormat = DataFormat.Xml;
            request.AddHeader("Content-Type", "application/xml");
            request.AddHeader("User-Agent", _userAgent);

            // Build the request URL
            string requestUrl = string.Format("{0}/{1}/{2}.xml.aspx?", _baseUrl.TrimEnd('/'), type, endpoint);

            // Make sure we have a valid API key
            if (key != default(ApiKey))
            {
                request.AddParameter("keyId", key.KeyId);
                request.AddParameter("vCode", key.VerificationCode);
            }

            // Was a character ID supplied?
            if (characterId.HasValue)
            {
                request.AddParameter("characterId", characterId.Value);
            }

            RestClient client = new RestClient(requestUrl);

            client.AddHandler("text/xml", new Utils.RestSharp.XmlAttributeDeserializer());
            client.AddHandler("application/xml", new Utils.RestSharp.XmlAttributeDeserializer());

            IRestResponse<ApiBase<T>> response = await client.ExecuteTaskAsync<ApiBase<T>>(request);

            if (response.ErrorException != null || !string.IsNullOrEmpty(response.ErrorMessage))
            {
                throw new Exception(string.Format("Exception occurred while calling the EVE API. Endpoint: {0} || Error message: {1}", requestUrl, response.ErrorMessage), response.ErrorException);
            }

            if (response != null && response.Data != null)
            {
                return response.Data;
            }

            return default(ApiBase<T>);
        }

        public async Task<ApiBase<T>> ExecuteAsync<T>(EndpointType type, Endpoint endpoint, ApiKey key)
        {
            return await ExecuteAsync<T>(type, endpoint, key, null);
        }

        public async Task<ApiBase<T>> ExecuteAsync<T>(EndpointType type, Endpoint endpoint, long characterId)
        {
            return await ExecuteAsync<T>(type, endpoint, null, characterId);
        }

        public async Task<ApiBase<T>> ExecuteAsync<T>(EndpointType type, Endpoint endpoint)
        {
            return await ExecuteAsync<T>(type, endpoint, null, null);
        }
    }
}