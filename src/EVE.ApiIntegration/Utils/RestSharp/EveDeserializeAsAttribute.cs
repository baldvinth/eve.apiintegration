﻿using System;

namespace EVE.ApiIntegration.Utils.RestSharp
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class, Inherited = false)]
    public class EveDeserializeAsAttribute : Attribute
    {
        /// <summary>
        /// The name to use for the serialized element
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Sets if the property to Deserialize is an Attribute or Element (Default: false)
        /// </summary>
        public bool Attribute { get; set; }

        /// <summary>
        /// Value of a given property when multiple options have been given
        /// </summary>
        public string Value { get; set; }
    }
}