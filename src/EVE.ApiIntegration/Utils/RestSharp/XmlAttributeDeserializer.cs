﻿using RestSharp.Extensions;
using System.Reflection;
using System.Xml.Linq;

namespace EVE.ApiIntegration.Utils.RestSharp
{
    public class XmlAttributeDeserializer : EveXmlDeserializer
    {
        protected override object GetValueFromXml(XElement root, XName name, PropertyInfo prop)
        {
            bool isAttribute = false;
            string attributeValue = string.Empty;

            //Check for the DeserializeAs attribute on the property
            EveDeserializeAsAttribute options = prop.GetAttribute<EveDeserializeAsAttribute>();

            if (options != null)
            {
                name = options.Name ?? name;
                isAttribute = options.Attribute;
                attributeValue = options.Value;
            }

            if (isAttribute)
            {
                XAttribute attributeVal = null;
                if (!string.IsNullOrEmpty(attributeValue))
                {
                    attributeVal = GetAttributeByNameAndValue(root, name, attributeValue);
                }
                else
                {
                    attributeVal = GetAttributeByName(root, name);
                }

                if (attributeVal != null)
                {
                    return attributeVal.Value;
                }
            }

            return base.GetValueFromXml(root, name, prop);
        }
    }
}