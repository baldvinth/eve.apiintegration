﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;

namespace EVE.ApiIntegration.Utils
{
    public static class ConfigurationHelper
    {
        /// <summary>
        /// Retrieve application setting in the form of a string.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetAppSetting(string key)
        {
            string value = ConfigurationManager.AppSettings[key];

            if (string.IsNullOrEmpty(value))
                throw new NullReferenceException(string.Format("The value for '{0}' is empty or key is missing from web.config", key));

            return value;
        }

        /// <summary>
        /// Retrieve an e-mail address from application settings.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetAppSettingEmail(string key)
        {
            string value = GetAppSetting(key);

            if (!Regex.IsMatch(value, @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$", RegexOptions.IgnoreCase))
                throw new FormatException(string.Format("The value for key '{0}' in web.config is not a valid e-mail address.", key));

            return value;
        }

        /// <summary>
        /// Retrieve application setting in the form of an integer.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int GetAppSettingInt(string key)
        {
            return int.Parse(GetAppSetting(key));
        }

        /// <summary>
        /// Retrieve application setting in the form of boolean
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool GetAppSettingBool(string key)
        {
            return bool.Parse(GetAppSetting(key));
        }

    }
}