﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EVE.ApiIntegration
{
    public enum EndpointType
    {
        Account,
        Api,
        Char,
        Corp,
        Eve,
        Map,
        Server
    }

    public enum Endpoint
    {
        //Account
        AccountStatus,
        ApiKeyInfo,
        Characters,

        //Api
        CallList,

        // Char
        AccountBalance,
        AssetList,
        Blueprints,
        Bookmarks,
        CalendarEventAttendees,
        CharacterSheet,
        ChatChannels,
        ContactList,
        ContactNotifications,
        ContractBids,
        ContractItems,
        Contracts,
        FacWarStats,
        IndustryJobs,
        IndustryJobsHistory,
        KillLog,
        KillMails,
        Locations,
        MailBodies,
        MailMessages,
        MarketOrders,
        Medals,
        Notifications,
        NotificationTexts,
        PlanetaryColonies,
        PlanetaryLinks,
        PlanetaryPins,
        PlanetaryRoutes,
        Research,
        SkillInTraining,
        SkillQueue,
        UpcomingCalendarEvents,
        WalletJournal,
        WalletTransactions,

        // Eve
        CharacterAffiliation,
        CharacterInfo,
        CharacterName,
        ConquerableStationList,
        ErrorList,
        RefTypes,
        TypeName,

        // Map
        FacWarSystems,
        Jumps,
        Kills,
        Sovereignty,

        // Server
        ServerStatus
    }
}