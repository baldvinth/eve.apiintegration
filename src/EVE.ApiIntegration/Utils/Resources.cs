﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace EVE.ApiIntegration.Utils {
    public class Resources {
        public static string GetString(Assembly assm, string base_dir, string txt_filename) {
            string ret_val = null;
            string assm_name = assm.GetName().Name;
            string resource_name = String.Format("{0}.{1}.{2}", assm_name, base_dir, txt_filename);
            var s = assm.GetManifestResourceStream(resource_name);
            var r = new StreamReader(s);
            try { ret_val = r.ReadToEnd(); } catch (Exception ex) { Console.WriteLine("ERROR: Exception: {0} occurred when trying to read in {1} as assembly resource.", ex.Message, txt_filename); }
            return ret_val;
        }

        public static Stream GetStream(Assembly assm, string base_dir, string txt_filename) {
            Stream ret_val = null;
            string assm_name = assm.GetName().Name;
            string resource_name = String.Format("{0}.{1}.{2}", assm_name, base_dir, txt_filename);
            ret_val = assm.GetManifestResourceStream(resource_name);
            return ret_val;
        }

    }
}