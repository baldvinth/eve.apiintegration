﻿using EVE.ApiIntegration.Utils.RestSharp;

namespace EVE.ApiIntegration
{
    public class ServerStatus
    {
        [EveDeserializeAs(Name = "serverOpen")]
        public bool ServerOpen { get; set; }

        [EveDeserializeAs(Name = "onlinePlayers")]
        public long OnlinePlayers { get; set; }

        public ServerStatus()
        {
            ServerOpen = false;
            OnlinePlayers = 0;
        }
    }
}