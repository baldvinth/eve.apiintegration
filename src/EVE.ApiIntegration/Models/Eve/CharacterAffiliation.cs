﻿using EVE.ApiIntegration.Utils;
using EVE.ApiIntegration.Utils.RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;

namespace EVE.ApiIntegration.Models.Eve {
    public class CharacterAffiliation {

        [EveDeserializeAs(Name = "characterID", Attribute = true)]
        [XmlAttribute(AttributeName ="characterID")]
        /// <summary>
        /// ID number of the character.
        /// </summary>
        public int CharacterID { get; set; }

        [EveDeserializeAs(Name = "characterName", Attribute = true)]
        [XmlAttribute(AttributeName = "characterName")]
        /// <summary>
        /// Name of the character.
        /// </summary>
        public string CharacterName { get; set; }

        [EveDeserializeAs(Name = "corporationID", Attribute = true)]
        [XmlAttribute(AttributeName = "corporationID")]
        /// <summary>
        /// ID number of the character's current corporation.
        /// </summary>
        public int CorporationID { get; set; }

        [EveDeserializeAs(Name = "corporationName", Attribute = true)]
        [XmlAttribute(AttributeName = "corporationName")]
        /// <summary>
        /// Name of the character's current corporation.
        /// </summary>
        public string CorporationName { get; set; }

        [EveDeserializeAs(Name = "allianceID", Attribute = true)]
        [XmlAttribute(AttributeName = "allianceID")]
        /// <summary>
        /// ID number of the character's current alliance.
        /// </summary>
        public int AllianceID { get; set; }

        [EveDeserializeAs(Name = "allianceName", Attribute = true)]
        [XmlAttribute(AttributeName = "allianceName")]
        /// <summary>
        /// Name of the character's current alliance.
        /// </summary>
        public string AllianceName { get; set; }

        [EveDeserializeAs(Name = "factionID", Attribute = true)]
        [XmlAttribute(AttributeName = "factionID")]
        /// <summary>
        /// ID number of the character's current faction.
        /// </summary>
        public int FactionID { get; set; }

        [EveDeserializeAs(Name = "factionName", Attribute = true)]
        [XmlAttribute(AttributeName = "factionName")]
        /// <summary>
        /// Name of the character's current faction.
        /// </summary>
        public string FactionName { get; set; }

        public CharacterAffiliation() { }

        protected static readonly string resourceUriKey = "characteraffiliation.uri";

        public static CharacterAffiliation Get(long id) {
            CharacterAffiliation ret_val = null;
            var idList = new List<long>(new long[] { id });
            var result = GetCharacterAffiliationList(idList);
            if (result != null)
                ret_val = result.FirstOrDefault();
            return ret_val;
        }

        public static List<CharacterAffiliation> Get(List<long> idList) {
            List<CharacterAffiliation> ret_val = null;
            ret_val = GetCharacterAffiliationList(idList);
            return ret_val;
        }

        private static List<CharacterAffiliation> GetCharacterAffiliationList(List<long> idList) {
            List<CharacterAffiliation> ret_val = null;
            if (idList != null && idList.Count > 0) {
                HttpClient client = new HttpClient();
                var ids = String.Join(",", idList);
                string characterAffiliationServiceUrl = ServiceInfo.Item(resourceUriKey);
                string characterAffiliationResourceUri = String.Format(characterAffiliationServiceUrl, ids);
                var getCharacterAffiliationTask = client.GetAsync(characterAffiliationResourceUri)
                    .ContinueWith((taskwithresponse) => {
                        // Get response data for CharacterAffiliation
                        CharacterAffiliationCollection _characterAffiliations = null;
                        if (taskwithresponse.Result.IsSuccessStatusCode) {
                            var result = taskwithresponse.Result;
                            var readtask = result.Content.ReadAsStreamAsync();
                            readtask.Wait();
                            if (readtask.Result.CanSeek) readtask.Result.Position = 0;
                            XmlReader reader = XmlReader.Create(readtask.Result);
                            reader.MoveToContent();

                            // Load the style sheet.
                            Stream xsl_res = Resources.GetStream(Assembly.GetExecutingAssembly(), "Models.Eve", "CharacterAffiliation.xslt");
                            XmlReader xsr = new XmlTextReader(xsl_res);
                            XslCompiledTransform xslt = new XslCompiledTransform();
                            XsltSettings xs = new XsltSettings();
                            xslt.Load(xsr);

                            // Create the writer.
                            XmlWriterSettings settings = new XmlWriterSettings();
                            settings.ConformanceLevel = ConformanceLevel.Auto;                            
                            var xmlt = new MemoryStream();
                            XmlWriter writer = XmlWriter.Create(xmlt, settings);

                            // Execute the transformation.
                            xslt.Transform(reader, writer);
                            if (xmlt.CanSeek) xmlt.Position = 0;

                            var serializer = new XmlSerializer(typeof(CharacterAffiliationCollection));

                            _characterAffiliations = (CharacterAffiliationCollection)serializer.Deserialize(xmlt);

                            writer.Close();
                            reader.Close();
                            client.Dispose();
                        }
                        return _characterAffiliations;
                    });
                getCharacterAffiliationTask.Wait();
                if (getCharacterAffiliationTask.Result != null)
                    ret_val = new List<CharacterAffiliation>(getCharacterAffiliationTask.Result.CharacterAffiliations);
            }

            return ret_val;
        }
    }
}