﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
  exclude-result-prefixes="msxsl">
  <xsl:output method="xml" indent="no"/>
  <xsl:strip-space elements="*"/>
  <xsl:template match="/eveapi/result">
    <characterAffiliations><xsl:apply-templates select="rowset"/></characterAffiliations>
  </xsl:template>
  <xsl:template match="currentTime|cachedUntil" />
  <xsl:template match="rowset">
    <xsl:apply-templates select="row"/>
  </xsl:template>
  <xsl:template match="row">
    <characterAffiliation><xsl:copy-of select="@*" /></characterAffiliation>
  </xsl:template>
</xsl:stylesheet>