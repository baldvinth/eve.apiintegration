﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace EVE.ApiIntegration.Models.Eve {
    [Serializable()]
    [XmlRoot("characterAffiliations")]
    public class CharacterAffiliationCollection {
        [XmlElement("characterAffiliation")]
        public CharacterAffiliation[] CharacterAffiliations { get; set; }
    }

}
