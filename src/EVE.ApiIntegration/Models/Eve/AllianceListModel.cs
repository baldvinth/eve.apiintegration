﻿using EVE.ApiIntegration.Utils.RestSharp;
using System;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class AllianceList
    {
        [EveDeserializeAs(Name = "name", Attribute = true, Value = "alliances")]
        public List<Alliance> Alliances { get; set; }
    }

    /// <summary>
    /// Alliance used for listing up all the alliances in the game
    /// </summary>
    public class Alliance
    {
        [EveDeserializeAs(Attribute = true, Name = "name")]
        public string Name { get; set; }

        [EveDeserializeAs(Attribute = true, Name = "shortName")]
        public string ShortName { get; set; }

        [EveDeserializeAs(Attribute = true, Name = "allianceID")]
        public long AllianceId { get; set; }

        [EveDeserializeAs(Attribute = true, Name = "executorCorpID")]
        public long ExecutorCorpId { get; set; }

        [EveDeserializeAs(Attribute = true, Name = "memberCount")]
        public long MemberCount { get; set; }

        [EveDeserializeAs(Attribute = true, Name = "startDate")]
        public DateTime StartDate { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "memberCorporations")]
        public List<MemberCorporation> MemberCorporations { get; set; }

        public Alliance()
        {
            MemberCorporations = new List<MemberCorporation>();
        }
    }

    /// <summary>
    /// Corporation used for listing up all the corporations associated with an alliance
    /// </summary>
    public class MemberCorporation
    {
        [EveDeserializeAs(Attribute = true, Name = "corporationID")]
        public long CorporationId { get; set; }

        [EveDeserializeAs(Attribute = true, Name = "startDate")]
        public DateTime StartDate { get; set; }
    }
}