﻿using EVE.ApiIntegration.Utils.RestSharp;
using System;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class CharacterInformation
    {
        [EveDeserializeAs(Name = "characterID")]
        public long CharacterId { get; set; }

        [EveDeserializeAs(Name = "characterName")]
        public string CharacterName { get; set; }

        [EveDeserializeAs(Name = "race")]
        public string Race { get; set; }

        [EveDeserializeAs(Name = "bloodlineID")]
        public int BloodlineId { get; set; }

        [EveDeserializeAs(Name = "bloodline")]
        public string Bloodline { get; set; }

        [EveDeserializeAs(Name = "ancestryID")]
        public int AncestryId { get; set; }

        [EveDeserializeAs(Name = "ancestry")]
        public string Ancestry { get; set; }

        [EveDeserializeAs(Name = "accountBalance")]
        public decimal AccountBalance { get; set; }

        [EveDeserializeAs(Name = "skillPoints")]
        public long SkillPoints { get; set; }

        [EveDeserializeAs(Name = "nextTrainingEnds")]
        public DateTime NextTrainingEnds { get; set; }

        [EveDeserializeAs(Name = "shipName")]
        public string ShipName { get; set; }

        [EveDeserializeAs(Name = "shipTypeID")]
        public long ShipTypeId { get; set; }

        [EveDeserializeAs(Name = "shipTypeName")]
        public string ShipTypeName { get; set; }

        [EveDeserializeAs(Name = "corporationID")]
        public decimal CorporationId { get; set; }

        [EveDeserializeAs(Name = "corporation")]
        public string CorporationName { get; set; }

        [EveDeserializeAs(Name = "corporationDate")]
        public DateTime CorporationJoinDate { get; set; }

        [EveDeserializeAs(Name = "lastKnownLocation")]
        public string LastKnownLocation { get; set; }

        [EveDeserializeAs(Name = "securityStatus")]
        public decimal SecurityStatus { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "employmentHistory")]
        public List<EmploymentHistory> EmploymentHistory { get; set; }

        public CharacterInformation()
        {
            EmploymentHistory = new List<EmploymentHistory>();
        }
    }

    public class EmploymentHistory
    {
        [EveDeserializeAs(Name = "recordID", Attribute = true)]
        public long RecordId { get; set; }

        [EveDeserializeAs(Name = "corporationID", Attribute = true)]
        public long CorporationId { get; set; }

        [EveDeserializeAs(Name = "corporationName", Attribute = true)]
        public string CorporationName { get; set; }

        [EveDeserializeAs(Name = "startDate", Attribute = true)]
        public DateTime StartDate { get; set; }
    }
}