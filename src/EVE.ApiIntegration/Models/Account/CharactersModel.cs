﻿using EVE.ApiIntegration.Utils.RestSharp;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class CharacterList
    {
        [EveDeserializeAs(Name = "name", Attribute = true, Value = "characters")]
        public List<Character> Characters { get; set; }

        public CharacterList()
        {
            Characters = new List<Character>();
        }
    }

    public class Character
    {
        [EveDeserializeAs(Name = "characterID", Attribute = true)]
        public long CharacterId { get; set; }

        [EveDeserializeAs(Name = "characterName", Attribute = true)]
        public string CharacterName { get; set; }

        [EveDeserializeAs(Name = "corporationID", Attribute = true)]
        public long CorporationId { get; set; }

        [EveDeserializeAs(Name = "corporationName", Attribute = true)]
        public string CorporationName { get; set; }

        [EveDeserializeAs(Name = "allianceID", Attribute = true)]
        public long AllianceId { get; set; }

        [EveDeserializeAs(Name = "allianceName", Attribute = true)]
        public string AllianceName { get; set; }

        [EveDeserializeAs(Name = "factionID", Attribute = true)]
        public long FactionId { get; set; }

        [EveDeserializeAs(Name = "factionName", Attribute = true)]
        public string FactionName { get; set; }
    }
}