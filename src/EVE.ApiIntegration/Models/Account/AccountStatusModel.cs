﻿using EVE.ApiIntegration.Utils.RestSharp;
using System;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class AccountStatus
    {
        [EveDeserializeAs(Name = "paidUntil")]
        public DateTime PaidUntil { get; set; }

        [EveDeserializeAs(Name = "createDate")]
        public DateTime CreateDate { get; set; }

        [EveDeserializeAs(Name = "logonCount")]
        public long LogonCount { get; set; }

        [EveDeserializeAs(Name = "logonMinutes")]
        public long LogonMinutes { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "multiCharacterTraining")]
        public List<MultiCharacterTraining> MultiCharacterTraining { get; set; }

        public AccountStatus()
        {
            MultiCharacterTraining = new List<MultiCharacterTraining>();
        }
    }

    public class MultiCharacterTraining
    {
        [EveDeserializeAs(Name = "trainingEnd", Attribute = true)]
        public DateTime TrainingEnds { get; set; }
    }
}