﻿using EVE.ApiIntegration.Utils.RestSharp;
using System;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class ApiKeyInfo
    {
        [EveDeserializeAs(Name = "accessMask", Attribute = true)]
        public long AccessMask { get; set; }

        [EveDeserializeAs(Name = "type", Attribute = true)]
        public ApiKeyType Type { get; set; }

        [EveDeserializeAs(Name = "expires", Attribute = true)]
        public DateTime? Expires { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "characters")]
        public List<Character> Characters { get; set; }

        public ApiKeyInfo()
        {
            Characters = new List<Character>();
        }
    }
}