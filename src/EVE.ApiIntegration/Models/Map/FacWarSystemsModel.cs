﻿using EVE.ApiIntegration.Utils.RestSharp;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class FacWarSystems
    {
        [EveDeserializeAs(Name = "name", Attribute = true, Value = "solarSystems")]
        public List<FacWarSystem> SolarSystems { get; set; }

        public FacWarSystems()
        {
            SolarSystems = new List<FacWarSystem>();
        }
    }

    public class FacWarSystem
    {
        [EveDeserializeAs(Name = "solarSystemID")]
        public long SolarSystemId { get; set; }

        [EveDeserializeAs(Name = "solarSystemName")]
        public string SolarSystemName { get; set; }

        [EveDeserializeAs(Name = "occupyingFactionID")]
        public long OccupyingFactionId { get; set; }

        [EveDeserializeAs(Name = "occupyingFactionName")]
        public string OccupyingFactionName { get; set; }

        [EveDeserializeAs(Name = "owningFactionID")]
        public long OwningFactionId { get; set; }

        [EveDeserializeAs(Name = "owningFactionName")]
        public string OwningFactionName { get; set; }

        [EveDeserializeAs(Name = "contested")]
        public bool Contested { get; set; }

        [EveDeserializeAs(Name = "victoryPoints")]
        public long VictoryPoints { get; set; }

        [EveDeserializeAs(Name = "victoryPointThreshold")]
        public long VictoryPointThreshold { get; set; }
    }
}