﻿using EVE.ApiIntegration.Utils.RestSharp;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class Kills
    {
        [EveDeserializeAs(Name = "name", Attribute = true, Value = "solarSystems")]
        public List<KillInSystem> SolarSystems { get; set; }

        public Kills()
        {
            SolarSystems = new List<KillInSystem>();
        }
    }

    public class KillInSystem
    {
        [EveDeserializeAs(Name = "solarSystemID")]
        public long SolarSystemId { get; set; }

        [EveDeserializeAs(Name = "shipKills")]
        public long ShipKills { get; set; }

        [EveDeserializeAs(Name = "factionKills")]
        public long FactionKills { get; set; }

        [EveDeserializeAs(Name = "podKills")]
        public long PodKills { get; set; }
    }
}