﻿using EVE.ApiIntegration.Utils.RestSharp;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class Jumps
    {
        [EveDeserializeAs(Name = "name", Attribute = true, Value = "solarSystems")]
        public List<JumpInSystem> SolarSystems { get; set; }

        public Jumps()
        {
            SolarSystems = new List<JumpInSystem>();
        }
    }

    public class JumpInSystem
    {
        [EveDeserializeAs(Name = "solarSystemID")]
        public long SolarSystemId { get; set; }

        [EveDeserializeAs(Name = "shipJumps")]
        public long ShipJumps { get; set; }
    }
}