﻿using EVE.ApiIntegration.Utils.RestSharp;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class Sovereignty
    {
        [EveDeserializeAs(Name = "name", Attribute = true, Value = "solarSystems")]
        public List<SovereignSystem> SolarSystems { get; set; }

        public Sovereignty()
        {
            SolarSystems = new List<SovereignSystem>();
        }
    }

    /// <summary>
    /// Information for star systems sovereignty
    /// </summary>
    public class SovereignSystem
    {
        [EveDeserializeAs(Name = "solarSystemID")]
        public long SolarSystemId { get; set; }

        [EveDeserializeAs(Name = "allianceID")]
        public string SolarSystemName { get; set; }

        [EveDeserializeAs(Name = "corporationID")]
        public long CorporationId { get; set; }

        [EveDeserializeAs(Name = "allianceID")]
        public long AllianceId { get; set; }

        [EveDeserializeAs(Name = "factionID")]
        public long FactionId { get; set; }
    }
}