﻿namespace EVE.ApiIntegration
{
    public class ApiKey
    {
        public long KeyId { get; set; }
        public string VerificationCode { get; set; }
        public ApiKeyInfo KeyInfo { get; set; }
    }

    public enum ApiKeyType
    {
        Account,
        Character,
        Corporation
    }
}