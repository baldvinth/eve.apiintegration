﻿using EVE.ApiIntegration.Utils.RestSharp;
using System;
using System.Xml.Serialization;

namespace EVE.ApiIntegration
{
    /// <summary>
    /// Base of every API call
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [XmlRoot("eveapi")]
    public class ApiBase<T>
    {
        [EveDeserializeAs(Name = "result")]
        public T Result { get; set; }

        [EveDeserializeAs(Name = "cachedUntil")]
        public DateTime CachedUntil { get; set; }

        [EveDeserializeAs(Name = "error")]
        public string ErrorMessage { get; set; }

        [EveDeserializeAs(Name = "code", Attribute = true)]
        public long? ErrorCode { get; set; }
    }
}