﻿using EVE.ApiIntegration.Utils.RestSharp;
using System;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class CharacterSheet
    {
        [EveDeserializeAs(Name = "characterID")]
        public long CharacterId { get; set; }

        [EveDeserializeAs(Name = "name")]
        public string CharacterName { get; set; }

        [EveDeserializeAs(Name = "homeStationID")]
        public long HomeStationId { get; set; }

        [EveDeserializeAs(Name = "DoB")]
        public DateTime DateOfBirth { get; set; }

        [EveDeserializeAs(Name = "race")]
        public string Race { get; set; }

        [EveDeserializeAs(Name = "bloodLineID")]
        public int BloodlineId { get; set; }

        [EveDeserializeAs(Name = "bloodLine")]
        public string Bloodline { get; set; }

        [EveDeserializeAs(Name = "ancestryID")]
        public int AncestryId { get; set; }

        [EveDeserializeAs(Name = "ancestry")]
        public string Ancestry { get; set; }

        [EveDeserializeAs(Name = "gender")]
        public string Gender { get; set; }

        [EveDeserializeAs(Name = "corporationName")]
        public string CorporationName { get; set; }

        [EveDeserializeAs(Name = "corporationID")]
        public long CorporationId { get; set; }

        [EveDeserializeAs(Name = "freeSkillPoints")]
        public long FreeSkillPoints { get; set; }

        [EveDeserializeAs(Name = "freeRespecs")]
        public long FreeRespecs { get; set; }

        [EveDeserializeAs(Name = "cloneJumpDate")]
        public DateTime JumpCloneDate { get; set; }

        [EveDeserializeAs(Name = "lastRespecDate")]
        public DateTime LastRespecDate { get; set; }

        [EveDeserializeAs(Name = "lastTimedRespec")]
        public DateTime LastTimedRespec { get; set; }

        [EveDeserializeAs(Name = "remoteStationDate")]
        public DateTime RemoteStationDate { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "jumpClones")]
        public List<JumpClone> JumpClones { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "jumpCloneImplants")]
        public List<JumpCloneImplant> JumpCloneImplants { get; set; }

        [EveDeserializeAs(Name = "jumpActivation")]
        public DateTime JumpActivation { get; set; }

        [EveDeserializeAs(Name = "jumpFatigue")]
        public DateTime JumpFatigue { get; set; }

        [EveDeserializeAs(Name = "jumpLastUpdate")]
        public DateTime JumpLastUpdate { get; set; }

        [EveDeserializeAs(Name = "balance")]
        public decimal Balance { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "implants")]
        public List<Implant> Implants { get; set; }

        [EveDeserializeAs(Name = "attributes")]
        public Attributes Attributes { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "skills")]
        public List<Skill> Skills { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "corporationRoles")]
        public List<Role> CorporationRoles { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "corporationRolesAtHQ")]
        public List<Role> CorporationRolesAtHq { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "corporationRolesAtBase")]
        public List<Role> CorporationRolesAtBase { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "corporationRolesAtOther")]
        public List<Role> CorporationRolesAtOther { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "corporationTitles")]
        public List<Role> CorporationTitles { get; set; }

        public CharacterSheet()
        {
            JumpClones = new List<JumpClone>();
            JumpCloneImplants = new List<JumpCloneImplant>();
            Implants = new List<Implant>();
            Skills = new List<Skill>();
            CorporationRoles = new List<Role>();
            CorporationRolesAtBase = new List<Role>();
            CorporationRolesAtHq = new List<Role>();
            CorporationRolesAtOther = new List<Role>();
            CorporationTitles = new List<Role>();
        }
    }

    public class Title
    {
        [EveDeserializeAs(Name = "titleID", Attribute = true)]
        public long TitleId { get; set; }

        [EveDeserializeAs(Name = "titleName", Attribute = true)]
        public string TitleName { get; set; }
    }

    public class Role
    {
        [EveDeserializeAs(Name = "roleID", Attribute = true)]
        public long RoleId { get; set; }

        [EveDeserializeAs(Name = "roleName", Attribute = true)]
        public string RoleName { get; set; }
    }

    public class Skill
    {
        [EveDeserializeAs(Name = "typeID", Attribute = true)]
        public long TypeId { get; set; }

        [EveDeserializeAs(Name = "skillpoints", Attribute = true)]
        public long SkillPoints { get; set; }

        [EveDeserializeAs(Name = "level", Attribute = true)]
        public int Level { get; set; }

        [EveDeserializeAs(Name = "published", Attribute = true)]
        public bool Published { get; set; }
    }

    public class Attributes
    {
        [EveDeserializeAs(Name = "intelligence")]
        public int Intelligence { get; set; }

        [EveDeserializeAs(Name = "memory")]
        public int Memory { get; set; }

        [EveDeserializeAs(Name = "charisma")]
        public int Charisma { get; set; }

        [EveDeserializeAs(Name = "perception")]
        public int Perception { get; set; }

        [EveDeserializeAs(Name = "willpower")]
        public int Willpower { get; set; }
    }

    public class Implant
    {
        [EveDeserializeAs(Name = "typeID", Attribute = true)]
        public long TypeId { get; set; }

        [EveDeserializeAs(Name = "typeName", Attribute = true)]
        public string TypeName { get; set; }
    }

    public class JumpClone
    {
        [EveDeserializeAs(Name = "jumpCloneID", Attribute = true)]
        public long JumpCloneId { get; set; }

        [EveDeserializeAs(Name = "typeID", Attribute = true)]
        public long TypeId { get; set; }

        [EveDeserializeAs(Name = "locationID", Attribute = true)]
        public long LocationId { get; set; }

        [EveDeserializeAs(Name = "cloneName", Attribute = true)]
        public string CloneName { get; set; }
    }

    public class JumpCloneImplant
    {
        [EveDeserializeAs(Name = "jumpCloneID", Attribute = true)]
        public long JumpCloneId { get; set; }

        [EveDeserializeAs(Name = "typeID", Attribute = true)]
        public long TypeId { get; set; }

        [EveDeserializeAs(Name = "typeName", Attribute = true)]
        public string TypeName { get; set; }
    }
}