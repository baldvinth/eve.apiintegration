﻿using EVE.ApiIntegration.Utils.RestSharp;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class AccountBalance
    {
        [EveDeserializeAs(Name = "name", Attribute = true, Value = "accounts")]
        public List<WalletAccount> Accounts { get; set; }

        public AccountBalance()
        {
            Accounts = new List<WalletAccount>();
        }
    }

    public class WalletAccount
    {
        [EveDeserializeAs(Name = "accountID", Attribute = true)]
        public long AccountId { get; set; }

        [EveDeserializeAs(Name = "accountKey", Attribute = true)]
        public int AccountKey { get; set; }

        [EveDeserializeAs(Name = "balance", Attribute = true)]
        public decimal Balance { get; set; }
    }
}