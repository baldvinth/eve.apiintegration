﻿using EVE.ApiIntegration.Utils.RestSharp;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class ContactList
    {
        [EveDeserializeAs(Name = "name", Attribute = true, Value = "contactList")]
        public List<Contact> Contacts { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "contactLabels")]
        public List<ContactLabel> Labels { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "corporateContactList")]
        public List<Contact> CorporationContacts { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "corporateContactLabels")]
        public List<ContactLabel> CorporationLabels { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "allianceContactList")]
        public List<Contact> AllianceContacts { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "allianceContactLabels")]
        public List<ContactLabel> AllianceLabels { get; set; }

        public ContactList()
        {
            Contacts = new List<Contact>();
            Labels = new List<ContactLabel>();
            CorporationContacts = new List<Contact>();
            CorporationLabels = new List<ContactLabel>();
            AllianceContacts = new List<Contact>();
            AllianceLabels = new List<ContactLabel>();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Contact
    {
        [EveDeserializeAs(Name = "contactID", Attribute = true)]
        public long ContactId { get; set; }

        [EveDeserializeAs(Name = "contactName", Attribute = true)]
        public string ContactName { get; set; }

        [EveDeserializeAs(Name = "standing", Attribute = true)]
        public int Standing { get; set; }

        [EveDeserializeAs(Name = "contactTypeID", Attribute = true)]
        public long ContactTypeId { get; set; }

        [EveDeserializeAs(Name = "labelMask", Attribute = true)]
        public int LabelMask { get; set; }

        [EveDeserializeAs(Name = "inWatchlist", Attribute = true)]
        public bool InWatchList { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ContactLabel
    {
        [EveDeserializeAs(Name = "name", Attribute = true)]
        public string Name { get; set; }
    }
}