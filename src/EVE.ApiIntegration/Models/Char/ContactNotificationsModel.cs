﻿using EVE.ApiIntegration.Utils.RestSharp;
using System;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class ContactNotificationList
    {
        [EveDeserializeAs(Name = "name", Attribute = true, Value = "contactNotifications")]
        public List<ContactNotification> ContactNotifications { get; set; }

        public ContactNotificationList()
        {
            ContactNotifications = new List<ContactNotification>();
        }
    }

    public class ContactNotification
    {
        [EveDeserializeAs(Name = "notificationID", Attribute = true)]
        public long NotificationId { get; set; }

        [EveDeserializeAs(Name = "senderID", Attribute = true)]
        public long SenderId { get; set; }

        [EveDeserializeAs(Name = "senderName", Attribute = true)]
        public string SenderName { get; set; }

        [EveDeserializeAs(Name = "sentDate", Attribute = true)]
        public DateTime SentDate { get; set; }

        [EveDeserializeAs(Name = "messageData", Attribute = true)]
        public string Message { get; set; }
    }
}