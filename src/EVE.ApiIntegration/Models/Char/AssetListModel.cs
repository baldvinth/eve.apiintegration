﻿using EVE.ApiIntegration.Utils.RestSharp;
using System.Collections.Generic;

namespace EVE.ApiIntegration
{
    public class AssetList
    {
        [EveDeserializeAs(Name = "name", Attribute = true, Value = "assets")]
        public List<AssetListItem> Assets { get; set; }

        public AssetList()
        {
            Assets = new List<AssetListItem>();
        }
    }

    public class AssetListItem
    {
        [EveDeserializeAs(Name = "itemID", Attribute = true)]
        public long ItemId { get; set; }

        [EveDeserializeAs(Name = "locationID", Attribute = true)]
        public long? LocationId { get; set; }

        [EveDeserializeAs(Name = "typeID", Attribute = true)]
        public int TypeId { get; set; }

        [EveDeserializeAs(Name = "quantity", Attribute = true)]
        public long Quantity { get; set; }

        [EveDeserializeAs(Name = "flag", Attribute = true)]
        public int Flag { get; set; }

        [EveDeserializeAs(Name = "singleton", Attribute = true)]
        public bool Singleton { get; set; }

        [EveDeserializeAs(Name = "rawQuantity", Attribute = true)]
        public int? RawQuantity { get; set; }

        [EveDeserializeAs(Name = "name", Attribute = true, Value = "contents")]
        public List<AssetListItem> Contents { get; set; }

        public AssetListItem()
        {
            Contents = new List<AssetListItem>();
        }
    }
}