﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Configuration;
using log4net;

namespace EVE.ApiIntegration {
    public static class ServiceInfo {
        public const string URI_SERVER_KEY = "EVE.ApiIntegration.Server";
        public const string URI_SITE_KEY = "EVE.ApiIntegration.Site";
        public const string URI_RESOURCES_KEY = "UriResources";
        private const string URI_PATH_SEPERATOR = "/";
        private static ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static IDictionary _urlCollection = null;
        private static Uri _baseUri;
        private static string _servicePath;
        private static Uri _serviceRoot;
        static ServiceInfo() {
            try {
                var uri = ConfigurationManager.AppSettings[URI_SERVER_KEY];
                _baseUri = new Uri(uri);
            } catch (Exception ex) {
                logger.Error("An error occurred trying to retrieve EVE.ApiIntegration.Server service location, from configuration file", ex);
                throw;
            }
            try {
                _servicePath = ConfigurationManager.AppSettings[URI_SITE_KEY];
            } catch (Exception ex) {
                logger.Error("An error occurred trying to retrieve EVE.ApiIntegration.Site, service site, from configuration file", ex);
                throw;
            }
            _serviceRoot = new Uri(_baseUri, _servicePath);
            try {
                _urlCollection = (IDictionary)ConfigurationManager.GetSection(URI_RESOURCES_KEY);
            } catch (Exception ex) {
                logger.Error("An error occurred trying to retrieve resource URL(s) from configuration file", ex);
                throw;
            }
        }        
        public static string Item(string urlkey)   
        {
            string ret_val = null;
            if (_urlCollection.Contains(urlkey))
                ret_val = _serviceRoot.AbsoluteUri.ToString() + URI_PATH_SEPERATOR + (string)_urlCollection[urlkey];
            return ret_val;
        }
    }
}
